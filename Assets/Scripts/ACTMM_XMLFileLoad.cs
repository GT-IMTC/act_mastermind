﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

public class ACTMM_XMLFileLoad : MonoBehaviour {

    public TextAsset GameAsset;
    List<Dictionary<string, string>> levels = new List<Dictionary<string, string>>();
    Dictionary<string,string> obj;
    Dictionary<string, int[]> dataobj;
    XmlNodeList savedXML;

    void Start()
    {
        GetLevel();
    }

    void Update()
    {
        //if (Input.GetKeyDown("0"))
        //{
        //    foreach (Dictionary<string, string> x in levels)
        //    {
        //        Debug.Log("("+x.Keys+", "+x.Values+")");
        //    }
        //}
    }

    public void GetLevel()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(GameAsset.text);
        XmlNodeList levelsList = xmlDoc.GetElementsByTagName("level");

        foreach (XmlNode levelInfo in levelsList)
        {
            XmlNodeList levelcontent = levelInfo.ChildNodes;
            obj = new Dictionary<string, string>();

            foreach (XmlNode levelValues in levelcontent)
            {
                if(levelValues.Name.Equals("guessingrounds"))
                {
                    obj.Add("guessingrounds", levelValues.InnerText);
                }

                if (levelValues.Name.Equals("rowcapacity"))
                {
                    obj.Add("rowcapacity", levelValues.InnerText);
                }

                if (levelValues.Name.Equals("colordifficulty"))
                {
                    obj.Add("colordifficulty", levelValues.InnerText);
                }
            }
            levels.Add(obj);
        }
        savedXML = levelsList; 
    }

    /*
    public void WriteData()
    {
        string filepath = Application.dataPath + @"/Assets/XML_Levels/SaveFile";
        XmlDocument xmlDoc = new XmlDocument();

        if (File.Exists(filepath))
        {
            xmlDoc.Load(filepath);

            XmlElement elmRoot = xmlDoc.DocumentElement;

            elmRoot.RemoveAll(); // remove all inside the transforms node.

            foreach (XmlNode level in savedXML)
            {
                XmlNodeList levelcontent = level.ChildNodes;
                XmlElement elmNew = xmlDoc.CreateElement("level");
                XmlElement gr;
                XmlElement rc;
                XmlElement cd;
                foreach (XmlNode levelValues in levelcontent)
                {


                    if (levelValues.Name.Equals("guessingrounds"))
                    {
                        gr = xmlDoc.CreateElement("guessingrounds");
                        gr.InnerText = levelValues.InnerText;
                    }

                    if (levelValues.Name.Equals("rowcapacity"))
                    {
                        rc = xmlDoc.CreateElement("rowcapacity");
                        rc.InnerText = levelValues.InnerText;
                    }

                    if (levelValues.Name.Equals("colordifficulty"))
                    {
                        cd = xmlDoc.CreateElement("colordifficulty");
                        cd.InnerText = levelValues.InnerText;
                    }
                }
                if (gr != null)
                    elmNew.AppendChild(gr);
                if (rc != null)
                    elmNew.AppendChild(rc);
                if (cd != null)
                    elmNew.AppendChild(cd);

                elmRoot.AppendChild(elmNew);
            }

            XmlElement data = xmlDoc.CreateElement("data"); // create the rotation node.

            foreach()
            {
                XmlElement levelData = xmlDoc.CreateElement("leveldata"); // create the x node.
            }



            XmlElement rotation_Y = xmlDoc.CreateElement("y"); // create the y node.
            rotation_Y.InnerText = y; // apply to the node text the values of the variable.

            XmlElement rotation_Z = xmlDoc.CreateElement("z"); // create the z node.
            rotation_Z.InnerText = z; // apply to the node text the values of the variable.

            elmNew.AppendChild(rotation_X); // make the rotation node the parent.
            elmNew.AppendChild(rotation_Y); // make the rotation node the parent.
            elmNew.AppendChild(rotation_Z); // make the rotation node the parent.
            elmRoot.AppendChild(elmNew); // make the transform node the parent.

            xmlDoc.Save(filepath); // save file.
        }
    }*/

    public void StoreRowData(string rowName, int[] data)
    {
        dataobj.Add(rowName, data);
    }

    public List<Dictionary<string,string>> RetrieveXML()
    {
        return levels;
    }
}
