﻿using UnityEngine;
using System.Collections;

public class ACTMM_LevelButton : MonoBehaviour {

    public int parentlevelID;
    private GameObject BoardGenerator;
    private GUIText text;
   
    void Start()
    {
        BoardGenerator = FindObjectOfType<ACTMM_BoardGenerator>().gameObject;
        text = GameObject.FindGameObjectWithTag("LevelNameText").GetComponent<GUIText>();
    }

    void ForcedInitialization(int i)
    {
        if (parentlevelID == i)
        {
            BoardGenerator.SendMessage("LoadMastermindBoard", this);
        }
    }

    void OnMouseDown()
    {
        BoardGenerator.SendMessage("LoadMastermindBoard", this);
        text.text = "Level " + parentlevelID;
    }

}
