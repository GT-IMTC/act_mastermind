﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ACTMM_BoardGenerator : MonoBehaviour {

    public int guessingRounds, rowCapacity, colorDifficulty;
    public float totalTimeLeft;
    private float displayedTime;
    public ACTMM_MasterPeg[] ColorOptions;
    public GameObject RowPrefab, Mastermind, CursorObject;
    public GameObject LeftHole, MiddleHole, RightHole, SimilarPeg, CongruentPeg;
    private GUIText TimerText;
    private Vector3 rowLocation, initialRowLocation, initialCursorLocation;
    private ACTMM_AIDirector MastermindScript;
    private ACTMM_XMLFileLoad FileLoader;
    List<Dictionary<string, string>> xmlValues;
    public int[] levelKeyList;
    public static int currentLevelNum;
    public int MaxNumberOfLevels;
    private ACTMM_LevelButton[] levelbuttons;

	// Use this for initialization
	void Start () {
        //Do not change this set up
        ColorOptions = new ACTMM_MasterPeg[8];
        FileLoader = GetComponent<ACTMM_XMLFileLoad>();
        InitializeColorOptions();
        initialRowLocation = new Vector3(-1.679209f, -3.578912f, -0.009277344f);
        initialCursorLocation = new Vector3(initialRowLocation.x - 0.820791f, initialRowLocation.y, initialRowLocation.z);
        rowLocation = initialRowLocation;
        MastermindScript = FindObjectOfType<ACTMM_AIDirector>();
        TimerText = GameObject.FindGameObjectWithTag("TimerText").GetComponent<GUIText>();
        Mastermind = MastermindScript.gameObject;
        levelKeyList = new int[MaxNumberOfLevels];
        xmlValues = FileLoader.RetrieveXML();

        
        //hard-coded default auto-initialization (only use for display purposes)

        //rowCapacity = 4;
        //guessingRounds = 10;
        //colorDifficulty = 8;
        //currentLevelNum = 1;
        //MastermindScript.mmChangeActiveRow(MastermindScript.RowList[0]);
        //MastermindScript.RoundCursor.transform.position = initialCursorLocation;

        //levelbuttons = FindObjectsOfType<ACTMM_LevelButton>();
        //foreach (ACTMM_LevelButton lb in levelbuttons)
        //{
        //    lb.gameObject.SendMessage("ForcedInitialization", 1);
        //}

	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        if (totalTimeLeft > 0)
        {
            totalTimeLeft -= Time.deltaTime / 60f;
            displayedTime = Mathf.Floor(totalTimeLeft*100f)/100f;
            TimerText.text = displayedTime.ToString("00.00");
        }
        else
        {
            totalTimeLeft = 0;
            TimerText.text = "" + totalTimeLeft;
        }
	}


    public void LoadMastermindBoard(ACTMM_LevelButton i)
    {
        if (totalTimeLeft > 0)
        {
            int num;
            
            //if there is a level currently loaded then the level is preserved before quitting
            if (currentLevelNum > 0)
            {
                num = MastermindScript.PreserveLevel(currentLevelNum - 1);
                levelKeyList[num] = num;
            }

            //if the level is not saved yet
            if (levelKeyList[i.parentlevelID] == null || levelKeyList[i.parentlevelID] == 0)
            {
                //change currentLevelNum to button pressed
                currentLevelNum = i.parentlevelID;

                //retrieve XML values
                try 
                {
                    string xmlGuessingRounds = "";
                    xmlValues[i.parentlevelID - 1].TryGetValue("guessingrounds", out xmlGuessingRounds);

                    string xmlRowCapacity = "";
                    xmlValues[i.parentlevelID - 1].TryGetValue("rowcapacity", out xmlRowCapacity);

                    string xmlColorDifficulty = "";
                    xmlValues[i.parentlevelID - 1].TryGetValue("colordifficulty", out xmlColorDifficulty);

                    //create the board and generate it from xml
					CreateBoard(Convert.ToInt32(xmlGuessingRounds), Convert.ToInt32(xmlRowCapacity), Convert.ToInt32(xmlColorDifficulty));
                    GenerateBoardPattern(Convert.ToInt32(xmlRowCapacity), Convert.ToInt32(xmlColorDifficulty));
                }
                catch //if values not available in the xml, defaults to default parameters: (10,4,8)
                {
                    Debug.Log("Error: Default parameters chosen, XML Levels number running out of bounds");
                    CreateBoard(guessingRounds, rowCapacity, colorDifficulty);
                    GenerateBoardPattern(rowCapacity, colorDifficulty);
                }

                //level is saved after loading in
                num = MastermindScript.PreserveLevel(currentLevelNum-1);

                //num is stored to authenticate loaded & saved levels from untouched ones
                levelKeyList[num] = num;
            }

            //load successfully saved levels
            ToggleColorOptions(MastermindScript.LoadLevel(i.parentlevelID-1, i));
            
            //change current level to button pressed
            currentLevelNum = i.parentlevelID;
        }
    }

    //creates a new combination
    public void GenerateBoardPattern(int rowcapacity, int colorNum)
    {
        MastermindScript.GeneratePattern(rowcapacity, colorNum);
        MastermindScript.RowInitialize();
    }

    //creates a new board
    public void CreateBoard(int rounds, int rowcapacity, int colorNum)
    {
        //clean up old data
        MastermindScript.RowList = new ACTMM_RowController[rounds];
        rowLocation = initialRowLocation;
        CursorObject.transform.position = initialCursorLocation;
        
        ACTMM_RowController row;
        
        //resets row length
        ACTMM_AIDirector.rowsRemaining = 1;
        for (int i = 0; i < rounds; i++)
        {
            if (i == 0)
            {
                row = CreateRow(rowcapacity, true);
                row.gameObject.name = "level"+ACTMM_BoardGenerator.currentLevelNum+"row1";
                rowLocation = new Vector3(rowLocation.x, rowLocation.y + 0.7786111f, rowLocation.z);
                MastermindScript.RowList[0] = row;
                ACTMM_AIDirector.rowsRemaining++;
                continue;
            }
            row = CreateRow(rowcapacity,false);
            row.gameObject.name = "level"+ACTMM_BoardGenerator.currentLevelNum+"_row" + (i+1);
            rowLocation = new Vector3(rowLocation.x, rowLocation.y + 0.7786111f, rowLocation.z);
            MastermindScript.RowList[i] = row;
            ACTMM_AIDirector.rowsRemaining++;
        }

        //resets cursor
        MastermindScript.RoundCursor.transform.position = initialCursorLocation;
        //resets row list
        MastermindScript.mmChangeActiveRow(MastermindScript.RowList[0]);  
        //resets remaing rounds
        ACTMM_AIDirector.rowsRemaining = rounds;
        //resets row length
        ACTMM_AIDirector.rowLength = rowcapacity;

        //set up possible color types
        for (int i = 0; i < 8; i++)
        {
            if (i < colorNum)
                ColorOptions[i].gameObject.SetActive(true);
            else
                ColorOptions[i].gameObject.SetActive(false);
        }
    }

    public ACTMM_RowController CreateRow(int capacity, bool activate)
    {
        //compose new row
        GameObject row = (GameObject)Instantiate(RowPrefab, rowLocation, Quaternion.identity);
        ACTMM_RowController rowScript = row.GetComponent<ACTMM_RowController>();

        //Initialize row script
        rowScript.HoleList = new ACTMM_HoleInfo[capacity];
        rowScript.CongruentHintPegs = new ACTMM_HintPegInfoHolder[capacity];
        rowScript.SimilarHintPegs = new ACTMM_HintPegInfoHolder[capacity];
        rowScript.Mastermind = this.Mastermind;


        GameObject hole;
        GameObject congruentPeg;
        GameObject similarPeg;

        if (capacity == 1)
        {
            //create middle hole
            hole = (GameObject) Instantiate(MiddleHole, row.transform.position, Quaternion.identity);
            hole.name = "level" + ACTMM_BoardGenerator.currentLevelNum + "Row" + ACTMM_AIDirector.rowsRemaining + "_hole1";
            rowScript.HoleList[0] = hole.GetComponent<ACTMM_HoleInfo>();

            //hide sequential rows
            if (!activate)
            {
                rowScript.DeactivateHoles();
            }

            //create hint pegs
            congruentPeg = (GameObject)Instantiate(CongruentPeg, new Vector3(0.8927175f + row.transform.position.x, -3.986846f + row.transform.position.y, 0), Quaternion.identity);
            rowScript.CongruentHintPegs[0] = congruentPeg.GetComponent<ACTMM_HintPegInfoHolder>();

            similarPeg = (GameObject)Instantiate(SimilarPeg, new Vector3(0.8927175f + row.transform.position.x, -3.986846f + row.transform.position.y, 0), Quaternion.identity);
            rowScript.SimilarHintPegs[0] = similarPeg.GetComponent<ACTMM_HintPegInfoHolder>();
            
            //hide hint pegs
            foreach (ACTMM_HintPegInfoHolder g in rowScript.SimilarHintPegs)
            {
                g.gameObject.SetActive(false);
            }
            foreach (ACTMM_HintPegInfoHolder g in rowScript.CongruentHintPegs)
            {
                g.gameObject.SetActive(false);
            }
            return rowScript;
        }

        for (int i = 0; i < capacity; i++)
        {
            //first hole of the row
            if (i == 0)
            {
                //create left hole
                hole = (GameObject)Instantiate(LeftHole, row.transform.position, Quaternion.identity);
                hole.name = "level" + ACTMM_BoardGenerator.currentLevelNum + "Row" + ACTMM_AIDirector.rowsRemaining + "_hole" + (i + 1);
                rowScript.HoleList[0] = hole.GetComponent<ACTMM_HoleInfo>();

                //create hint pegs
                congruentPeg = (GameObject)Instantiate(CongruentPeg, new Vector3(((capacity-1) * 0.71f) + 0.8927175f + row.transform.position.x, 0.085924f + row.transform.position.y, 0), Quaternion.identity);
                rowScript.CongruentHintPegs[0] = congruentPeg.GetComponent<ACTMM_HintPegInfoHolder>();

                similarPeg = (GameObject)Instantiate(SimilarPeg, new Vector3(((capacity-1) * 0.71f) + 0.8927175f + row.transform.position.x, 0.085924f + row.transform.position.y, 0), Quaternion.identity);
                rowScript.SimilarHintPegs[0] = similarPeg.GetComponent<ACTMM_HintPegInfoHolder>();
                continue;
            }

            //last hole of the row
            if (i + 1 == capacity)
            {
                //create right hole 
                hole = (GameObject)Instantiate(RightHole, new Vector3(row.transform.position.x + (i * 0.71f), row.transform.position.y, 0), Quaternion.identity);
                hole.name = "level" + ACTMM_BoardGenerator.currentLevelNum + "Row" + ACTMM_AIDirector.rowsRemaining + "_hole" + (i + 1);
                rowScript.HoleList[i] = hole.GetComponent<ACTMM_HoleInfo>();

                //create hint pegs
                congruentPeg = (GameObject)Instantiate(CongruentPeg, new Vector3(((capacity-1)*0.71f)+0.8927175f + row.transform.position.x + ((i / 2) * 0.3526885f), 0.085924f + (-0.285722f * (i % 2)) + row.transform.position.y, 0), Quaternion.identity);
                rowScript.CongruentHintPegs[i] = congruentPeg.GetComponent<ACTMM_HintPegInfoHolder>();

                similarPeg = (GameObject)Instantiate(SimilarPeg, new Vector3(((capacity-1) * 0.71f) + 0.8927175f + row.transform.position.x + ((i / 2) * 0.3526885f), 0.085924f + (-0.285722f * (i % 2)) + row.transform.position.y, 0), Quaternion.identity);
                rowScript.SimilarHintPegs[i] = similarPeg.GetComponent<ACTMM_HintPegInfoHolder>();
                continue;
            }
            //middle holes of the row
            //create middle hole
            hole = (GameObject)Instantiate(MiddleHole, new Vector3(row.transform.position.x+(i*0.71f),row.transform.position.y,0), Quaternion.identity);
            hole.name = "level" + ACTMM_BoardGenerator.currentLevelNum + "Row" + ACTMM_AIDirector.rowsRemaining + "_hole" + (i + 1);
            rowScript.HoleList[i] = hole.GetComponent<ACTMM_HoleInfo>();

            //create hint pegs

            congruentPeg = (GameObject)Instantiate(CongruentPeg, new Vector3(((capacity-1) * 0.71f) + 0.8927175f + row.transform.position.x + ((i / 2) * 0.3526885f), 0.085924f + (-0.285722f * (i % 2)) + row.transform.position.y, 0), Quaternion.identity);
            rowScript.CongruentHintPegs[i] = congruentPeg.GetComponent<ACTMM_HintPegInfoHolder>();

            similarPeg = (GameObject)Instantiate(SimilarPeg, new Vector3(((capacity-1) * 0.71f) + 0.8927175f + row.transform.position.x + ((i / 2) * 0.3526885f), 0.085924f + (-0.285722f * (i % 2)) + row.transform.position.y, 0), Quaternion.identity);
            rowScript.SimilarHintPegs[i] = similarPeg.GetComponent<ACTMM_HintPegInfoHolder>();
        }

        //hide sequential rows
        if (!activate)
        {
            rowScript.DeactivateHoles();
        }

        //hide hint pegs
        foreach (ACTMM_HintPegInfoHolder g in rowScript.SimilarHintPegs)
        {
            g.gameObject.SetActive(false);
        }
        foreach (ACTMM_HintPegInfoHolder g in rowScript.CongruentHintPegs)
        {
            g.gameObject.SetActive(false);
        }
        return rowScript;
    }

    //sorts the pegs in a list to control
    void InitializeColorOptions()
    {
        ACTMM_MasterPeg[] l = FindObjectsOfType<ACTMM_MasterPeg>();

        foreach (ACTMM_MasterPeg mp in l)
        {
            ColorOptions[mp.ColorID-1] = mp;
        }
    }

    void ToggleColorOptions(int colorNum)
    {
        //set up possible color types
        for (int i = 0; i < 8; i++)
        {
            if (i < colorNum)
                ColorOptions[i].gameObject.SetActive(true);
            else
                ColorOptions[i].gameObject.SetActive(false);
        }
    }

}
