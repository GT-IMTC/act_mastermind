﻿using UnityEngine;
using System.Collections;

//Wrapper class made to hold info about levels being loaded in
public class ACTMM_LevelInfoHolder {

    public ACTMM_RowController[] levelRowList;
    public Vector3 roundCursorLocation;
    public int ActiveRowNum;
    public int colorDifficultyLevel;
}
