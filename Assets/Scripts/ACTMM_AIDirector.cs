﻿using UnityEngine;
using System.Collections;

public class ACTMM_AIDirector : MonoBehaviour {

    public string Pattern;
    public static int[] ptnholder = new int[4];
    public static int rowsRemaining, rowNum;
    public ACTMM_RowController[] RowList;
    private ACTMM_CheckButtonBehavior CheckButton;
    public GameObject RoundCursor;
    private static int levelNum;
    public static int rowLength;
    public static ACTMM_LevelInfoHolder[] levelList;
    private ACTMM_LevelButton currentLevelButton;
    public int numberOfLevels;
    public int currentCodeLength;


	// Use this for initialization
	void Start () {
        //soft coded checkbutton functionality
        CheckButton = FindObjectOfType<ACTMM_CheckButtonBehavior>();
        levelList = new ACTMM_LevelInfoHolder[numberOfLevels];
        
	}
	
	// Update is called once per frame
	void Update () {

        ////change combination with keypress of 1
        //if (Input.GetKeyDown("1"))
        //{
        //    ptnholder = GeneratePattern(4);
        //    DecryptPattern(ptnholder);
        //    RowList[rowNum].ptnholder = ptnholder;
        //}
	}

    //Generates a new pattern into an int[]
    public int[] GeneratePattern(int length, int colorDifficulty)
    {
        if (colorDifficulty < 1)
            Debug.Log("Error: colorDifficulty selected less than 1");

        if (colorDifficulty > 8)
            Debug.Log("Error: colorDifficulty selected greater than 8");

        int x;
        int[] y = new int[length];

        for (int i = 0; i < length; i++)
        {
            x = Random.Range(1, colorDifficulty);
            y[i] = x;
        }
        ptnholder = y;
        DecryptPattern(y);
        currentCodeLength = colorDifficulty;
        return y;
    }

    //Decrypts the pattern into a string that is displayed on The Mastermind gameobject in the scene for debugging purposes
    private string DecryptPattern(int[] pattern)
    {
        Pattern = "";
        foreach (int i in pattern)
        {
            switch (i)
            {
                case 1:
                    Pattern = Pattern + "Black-";
                    break;
                case 2:
                    Pattern = Pattern + "Blue-";
                    break;
                case 3:
                    Pattern = Pattern + "Brown-";
                    break;
                case 4:
                    Pattern = Pattern + "Green-";
                    break;
                case 5:
                    Pattern = Pattern + "Orange-";
                    break;
                case 6:
                    Pattern = Pattern + "Red-";
                    break;
                case 7:
                    Pattern = Pattern + "White-";
                    break;
                case 8:
                    Pattern = Pattern + "Yellow-";
                    break;
                default:
                    Pattern = Pattern + "ERROR";
                    break;
            }
        }
        return Pattern;
    }

    public void RowInitialize()
    {
        RowList[rowNum].ptnholder = ptnholder;
    }


    public void mmChangeActiveRow(ACTMM_RowController r)
    {
        CheckButton.ChangeActiveRow(r);
    }

    //Changes from current row to next row in the scene
    public void RowIncrement()
    {
        //Debug.Log("past row: " + RowList[rowNum - 1]);
        //Debug.Log("current row: " + RowList[rowNum]);
        RowList[rowNum - 1].DeactivateHoles();
        RowList[rowNum].ActivateHoles();
        
        CheckButton.ChangeActiveRow(RowList[rowNum]);
        foreach (int i in ptnholder)
        {
            Debug.Log(i);
        }
        RowList[rowNum].ptnholder = ptnholder;
        RoundCursor.transform.position = new Vector3(RoundCursor.transform.position.x, RoundCursor.transform.position.y + 0.7786111f, RoundCursor.transform.position.z);
    }

    /*
     * Loads a Level from memory
     * int levelKey - id of the level being loaded from memory in levelList
     * return - level number it was saved as
     */
    public int PreserveLevel(int levelKey)
    {
        for (int i = 0; i < RowList.Length; i++)
        {
            RowList[i].SaveHoles();
            /*
            foreach (ACTMM_MasterPeg dPeg in dummyPegList)
            {
                allPeg
            }*/
        }

        levelNum++;
        //Debug.Log(levelList);
        //Debug.Log(levelList[levelNum]);
        levelList[levelKey] = new ACTMM_LevelInfoHolder();
        levelList[levelKey].levelRowList = RowList;
        levelList[levelKey].roundCursorLocation = RoundCursor.transform.position;
        levelList[levelKey].ActiveRowNum = rowNum;
        levelList[levelKey].colorDifficultyLevel = currentCodeLength;
        //zero row num in case blank level
        rowNum = 0;
        return levelKey+1;
    }

    public int LoadLevel(int ln, ACTMM_LevelButton lb)
    {
        //Debug.Log("level Loaded: "+ln);

		
        //for (int i = 0; i < levelList.Length; i++)
        //{
        //    Debug.Log("levels: " + i);

        //    if (levelList[i] != null)
        //    {
        //        foreach (ACTMM_RowController r in levelList[i].levelRowList)
        //        {
        //            Debug.Log("Row loaded: " + r.gameObject);
        //        }
        //    }
        //}
        
        currentLevelButton = lb;
        RowList = levelList[ln].levelRowList;
        for (int i = 0; i < RowList.Length; i++)
        {
            RowList[i].LoadHoles();
        }

        //resets cursor to saved location
        RoundCursor.transform.position = levelList[ln].roundCursorLocation;

        //resets rowNum to the last active row
        rowNum = levelList[ln].ActiveRowNum;
        ptnholder = RowList[rowNum].ptnholder;
        DecryptPattern(ptnholder);

        //Changes Active row for check button to be the new active row
        mmChangeActiveRow(RowList[levelList[ln].ActiveRowNum]);
        
        //changes current code length according to the saved code length
        currentCodeLength = levelList[ln].colorDifficultyLevel;

        return levelList[ln].colorDifficultyLevel;
    }

    void CorrectCombination()
    {
        SpriteRenderer sr = currentLevelButton.gameObject.GetComponent<SpriteRenderer>();
        sr.color = Color.black;
    }

}
