﻿using UnityEngine;
using System.Collections;

public class ACTMM_RowController : MonoBehaviour{

    public ACTMM_HoleInfo[] HoleList;
    public ACTMM_HintPegInfoHolder[] CongruentHintPegs, SimilarHintPegs;
    public int similar, congruent;
    public GameObject Mastermind;
    public int[] ptnholder = new int[4];
    public ACTMM_XMLFileLoad dataholder;
    public int[] userCombination;

    void Start()
    {
        dataholder = FindObjectOfType<ACTMM_XMLFileLoad>();
    }

    public void CheckRow()
    {

        int i = 0;
        similar = 0;
        congruent = 0;


        if (dataholder == null)
        {
            Debug.Log("Error: finding XML objet");
            dataholder = FindObjectOfType<ACTMM_XMLFileLoad>();
            //CheckRow();
        }
        else
        {
            userCombination = new int[ptnholder.Length];
            int j = 0;

            foreach (ACTMM_HoleInfo h in HoleList)
            {
                userCombination[j] = h.filledColorID;
                j++;
            }

            //dataholder.StoreRowData(this.gameObject.name, userCombination);
        }

        //congruency check
        foreach (ACTMM_HoleInfo h in HoleList)
        {
            //Debug.Log("name: "+this.gameObject);
            //Debug.Log("hole: " + h);
            //Debug.Log("hole color: " +h.filledColorID);
            //Debug.Log("i: " + i);
            //Debug.Log("ptn: " + ptnholder[i]);
            if (h.filledColorID == 0)
            {
                Debug.Log("ERROR: row hole not filled in");
                return;
            }
            if (h.filledColorID == ptnholder[i])
                congruent++;
            i++;
        }

        //similarity check
        int[] tempPattern = new int[4];
        tempPattern = (int[]) ptnholder.Clone();
        foreach (ACTMM_HoleInfo h in HoleList)
        {
            for (int j = 0; j < tempPattern.Length; j++)
            {
                //check the temporary array for similarities, discared similar info out of temp array for other runs
                if (h.filledColorID != 0 && h.filledColorID == tempPattern[j])
                {
                    similar++;
                    tempPattern[j] = 0;
                    break;
                }
                else if (h.filledColorID == 0)
                {
                    Debug.Log("ERROR: row hole not filled in");
                }
            }
            //disable peg dragging
            h.mPeg.enabled = false;
        }



        //increments row 
        ACTMM_AIDirector.rowsRemaining--;
        ACTMM_AIDirector.rowNum++;
        AssignHints();

        //Debug.Log(ACTMM_AIDirector.rowsRemaining +" > "+0);
        if (ACTMM_AIDirector.rowsRemaining > 0 && congruent < ptnholder.Length)
        {
            //Debug.Log("increment");
            Mastermind.SendMessage("RowIncrement");
        }
        else if (congruent >= ptnholder.Length)
        {
            Mastermind.SendMessage("CorrectCombination");
        }
    }

    //assigns hint pegs beside the combination
    public void AssignHints()
    {
        similar = similar - congruent;

        for (int i = 0; i < congruent; i++)
        {
            CongruentHintPegs[i].gameObject.SetActive(true);
        }

        for (int i = congruent; i < similar+congruent; i++)
        {
            SimilarHintPegs[i].gameObject.SetActive(true);
        }
    }

    //Deactivates the holes in the row
    //used to solidify the checked combination 
    public void DeactivateHoles()
    {
        foreach (ACTMM_HoleInfo h in HoleList)
        {
            h.enabled = false;
        }
    }

    //activates the holes in the row
    //used to increment to the next row of holes
    public void ActivateHoles()
    {
        foreach (ACTMM_HoleInfo h in HoleList)
        {
            h.enabled = true;
            //Debug.Log("hole name: " + h.gameObject);
            //Debug.Log("holes activated?: "+h.enabled);
        }
    }

    /*deactivates all the relevant holes in the rowList,
     *deactivates all the relevant pegs in the rowList,
     *deactivates all the relevant hint pegs in the rowList,
     *returns a list of relevant pegs to keep track of the cloneable Master Pegs in the scene
     */
    public void SaveHoles()
    {

        foreach (ACTMM_HoleInfo h in HoleList)
        {
            if (h.mPeg != null)
            {
                h.mPeg.gameObject.SetActive(false);
            }
            h.gameObject.SetActive(false);
        }

        foreach (ACTMM_HintPegInfoHolder g in SimilarHintPegs)
        {
            if (g.gameObject.activeSelf)
            {
                g.on = true;
            }

            g.gameObject.SetActive(false);
        }

        foreach (ACTMM_HintPegInfoHolder g in CongruentHintPegs)
        {
            if (g.gameObject.activeSelf)
            {
                g.on = true;
            }

            g.gameObject.SetActive(false);
        }

    }

    public void LoadHoles()
    {
        foreach (ACTMM_HoleInfo h in HoleList)
        {
            h.gameObject.SetActive(true);

            if (h.mPeg != null)
            {
                h.mPeg.gameObject.SetActive(true);
            }
        }

        foreach (ACTMM_HintPegInfoHolder g in SimilarHintPegs)
        {
            if (g.on)
            {
                g.gameObject.SetActive(true);
            }
        }

        foreach (ACTMM_HintPegInfoHolder g in CongruentHintPegs)
        {
            if (g.on)
            {
                g.gameObject.SetActive(true);
            }
        }
    }
}
