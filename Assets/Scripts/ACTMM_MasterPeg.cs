﻿using UnityEngine;
using System.Collections;

public class ACTMM_MasterPeg : MonoBehaviour {

    public float sensitivity;
    public bool validPos, dragged, ClonePeg;
    public Vector3 mousePos, movVector, snapPos, holePos;
    public int ColorID;
    public GameObject occupiedHole;
    private ACTMM_BoardGenerator BoardGenerator;
    private Camera mainCamera;

	// Use this for initialization
	void Start () {
        mainCamera = FindObjectOfType<Camera>();
        snapPos = this.gameObject.transform.position;
        sensitivity = 1;
        BoardGenerator = FindObjectOfType<ACTMM_BoardGenerator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (dragged)
            this.gameObject.transform.position = snapPos + (movVector/sensitivity);/*/sensitivity*);*/
	}

    //Picks up the peg
    //If this peg is a 'master peg' it will create a clone in its stead
    //If this peg is a 'cloned peg' it will not create a clone int its stead
    public void PegOnMouseDown()
    {
        if (!ClonePeg)
        {
            GameObject g = (GameObject)Instantiate(this.gameObject, this.gameObject.transform.position, this.gameObject.transform.rotation);
            BoardGenerator.ColorOptions[ColorID - 1] = g.GetComponent<ACTMM_MasterPeg>();
        }
        mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    //drags the peg during the holding of the mouse button based upon its initial location vs its displacement multiplied by its sensitivity
    public void PegOnMouseDrag()
    {
        movVector = mainCamera.ScreenToWorldPoint(Input.mousePosition)-mousePos;
        //movVector = Input.mousePosition - mousePos;
        dragged = true;
        //Debug.Log(Input.mousePosition);
    }

    //if the peg is in a valid Position it will undergo a series of commands to update the input combination and 'snap positon' of the peg
    // if the peg is in an invalid position it is destroyed
    public void PegOnMouseUp()
    {
        if (validPos)
        {
            snapPos = holePos;
            this.gameObject.transform.position = snapPos;
            dragged = false;
            ClonePeg = true;
            occupiedHole.SendMessage("HoleFilled", this);
        }
        else
        {
            Debug.Log(this.gameObject.transform.position);
            Destroy(this.gameObject);
        }

    }

    //This updates whether the peg is in a valid location and prepares to tell the hole to receive the ColorID
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Hole"))
        {
            validPos = true;
            occupiedHole = other.gameObject;
            holePos = other.gameObject.transform.position;
        }
    }

    //This updates whether the peg is touching a valid location
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Hole"))
        {
            validPos = false;
        }
    }
}
