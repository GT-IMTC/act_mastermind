﻿using UnityEngine;
using System.Collections;

public class ACTMM_CheckButtonBehavior : MonoBehaviour {

    public ACTMM_RowController activeRow;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //tells the corresponding row to check itself relative to the mastermind's combination
    void OnMouseDown()
    {
        activeRow.CheckRow();
    }

    //changes the active row based on the which row requires input
    public void ChangeActiveRow(ACTMM_RowController aRow)
    {
        activeRow = aRow;
    }
}
