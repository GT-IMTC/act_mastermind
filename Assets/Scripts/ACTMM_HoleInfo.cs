﻿using UnityEngine;
using System.Collections;

public class ACTMM_HoleInfo : MonoBehaviour {

    public bool filled;
    public int filledColorID;
    public ACTMM_MasterPeg mPeg;

	// Use this for initialization
	void Start () 
    {
        this.gameObject.tag = "Hole";
	}

    //private method accessed by collisions from the colored peg.
    //Any peg placed upon MouseUp will superscede the previous peg and the info will be held here
    void HoleFilled(ACTMM_MasterPeg peg)
    {
        if (mPeg != null)
        {
            Destroy(mPeg);
        }
        filledColorID = peg.ColorID;
        mPeg = peg;
    }


}
