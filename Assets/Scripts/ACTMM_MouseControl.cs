﻿using UnityEngine;
using System.Collections;

public class ACTMM_MouseControl : MonoBehaviour {

    private ACTMM_MasterPeg myPeg;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {

            Vector3 mousePoint = camera.ScreenToWorldPoint(Input.mousePosition);
            Collider2D[] colList = Physics2D.OverlapPointAll(new Vector2(mousePoint.x, mousePoint.y));

            foreach (Collider2D c in colList)
            {
                if (c.gameObject.tag.Equals("Peg"))
                {
                    ACTMM_MasterPeg mpg = c.GetComponent<ACTMM_MasterPeg>();
                    myPeg = mpg;
                    myPeg.PegOnMouseDown();
                }
            }

        }

        if(Input.GetMouseButton(0))
        {
			if(myPeg!=null)
            	myPeg.PegOnMouseDrag(); 
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (myPeg != null)
            {
                myPeg.PegOnMouseUp();
            }

            myPeg = null;
        }
	}
}
